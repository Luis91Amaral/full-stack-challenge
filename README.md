# Limehome Challenge - Software Engineer

Thanks for taking the time to solve our challenge.

The task is designed to assess your interest, problem-solving skills and experience. We want to see your code, your approach, and your talent.

## Our Challenge

Your task revolves around a simple example project:
> A prototype that allows booking of hotels near the current location.

We included an example screen to give you an idea:

![Map Design](design/example-screen.png)

Depending on your proposed **role**, you should only **focus** on specific parts of this prototype.

We expect most people to **spend between 6 and 8 hours** on this challenge, although you're free to use more time if you want.

So **keep it simple**, but also remember to **show off your skills**. We want to see what you can do.

We expect you to submit a **link to your code** as well as a **hosted version** (e.g. using Heroku, AWS or GitHub pages) of it. 

Now, **pick the focus depending on your role**:
- [Front-End Engineer](#focus-front-end-engineer)
- [Back-End Engineer](#focus-back-end-engineer)
- [Full-Stack Engineer](#focus-full-stack-engineer)

To get the hotels in the area, you can e.g. use [HERE API](https://developer.here.com/documentation/geocoding-search-api/dev_guide/topics/quick-start.html) or [Google Places API](https://developers.google.com/places/web-service/search).
You are free to pick any provider that you see as a good fit for the task at hand. 

If you have any questions or feedback about the challenge, don't hesitate to reach out to us: [tech-hiring@limehome.com](tech-hiring@limehome.com)

Good luck with the challenge! We are looking forward to your solution!

---

### Focus: Front-End Engineer

We challenge you to build a **user interface** for this prototype. 
It should follow the [design](./design/example-screen.png) as closely as possible.

Please focus on **mobile screen sizes** (with a minimum resolution of 320x480). Ideally it also works on larger devices, but this is optional. 

We included some assets (e.g. our logo) you can use: [Design Assets](./design/assets)

##### We expect:
- A movable map showing the hotels near the displayed location
- By default, the map should show the current device location (if available)
- A slider with the hotel details and a book button
- A basic booking form that shows a confirmation message on submission
  - No need to send the booking data anywhere

##### We check:
- Functionality
- UI look and feel (minimal but appealing, should follow provided screen)
- UI best practices (e.g. correct use of labels in the form)
- Code quality (e.g. code style and tests)
- Cross-browser support (at least iOS and Android)

##### Technology:
A single page application using a framework of your choice (e.g. Angular, React or Vue).

Please **avoid UI frameworks** like Bootstrap or Material UI as we want to see your CSS / JS skills.

---

### Focus: Back-End Engineer

We challenge you to build an API server for this prototype.
The API design is up to you. Build something that a front-end developer could integrate.

##### We expect:
- An endpoint that returns a **list of hotels** near given coordinates (e.g. `48.130323,11.576362`)
- An endpoint to **create a new booking** for a given hotel
- An endpoint to **list bookings** for a given hotel (no authentication needed)

##### What we check:
- Functionality
- API design (endpoint structure and request / response format)
- API documentation (e.g. OpenAPI or Postman)
- API robustness (e.g. validation)
- Code quality (e.g. code style and tests)

##### Technology:
We recommend JavaScript / TypeScript or Python, but you can pick a different language as well.

---

### Focus: Full-Stack Engineer

We challenge you to build a **full prototype** with both UI and API.
The UI and API should be separated as client and server applications. 

For the UI you can follow the [design](./design/Results%20page%20—%20Map.png) we provided, but you can also use a different style.

Please focus on **mobile screen sizes** (with a minimum resolution of 320x480). Ideally it also works on larger devices, but this is optional. 

We included some assets (e.g. our logo) you can use: [Design Assets](./design/assets)

##### We expect:
- A map showing hotels near the displayed location
- It should be possible to **select a hotel** to get further details
- A basic **booking form** that shows a confirmation message on submission
- A public **API endpoint**, listing submitted bookings for a given hotel

#### What we check:
- Functionality
- UI look and feel (minimal but appealing)
- API design (endpoint structure and request / response format)
- API robustness (e.g. validation)
- Code quality (e.g. code style and tests)

##### Technology:
The front-end should be a single page app using a major framework (e.g. Angular, React or Vue).

For the back-end we recommend JavaScript / TypeScript or Python, but you can pick a different language as well.
